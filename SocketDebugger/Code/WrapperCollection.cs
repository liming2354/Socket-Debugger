﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Text;
using SD.Common;

namespace SocketClient
{
    public class WrapperCollection : List<IWrapper>
    {
    }

    /// <summary>
    /// 
    /// </summary>
    public class WrapperManager
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="targetType"></param>
        /// <param name="interfaceType"></param>
        /// <returns></returns>
        static private bool IsImplementInterface(Type targetType, Type interfaceType)
        {
            Type[] types = targetType.GetInterfaces();
            foreach (Type t in types)
            {
                if (t == interfaceType)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public WrapperCollection WrapperCollection
        {
            get
            {
                if (_wrapperCollection == null)
                {
                    _wrapperCollection = new WrapperCollection();

                    // load from assembly
                    //
                    string path = Path.Combine(System.Windows.Forms.Application.StartupPath, "Wrap");

                    DirectoryInfo di = new DirectoryInfo(path);
                    FileInfo[] fileInfos = di.GetFiles("*.dll");

                    foreach (FileInfo fi in fileInfos)
                    {
                        Assembly a = null;
                        try
                        {
                            a = Assembly.LoadFrom(fi.FullName);
                        }
                        catch (Exception ex)
                        {
                            NUnit.UiKit.UserMessage.DisplayFailure(ex.Message);
                        }

                        foreach (Type tp in a.GetTypes())
                        {
                            if (IsImplementInterface(tp, typeof(IWrapper)))
                            {
                                object obj = null;
                                try
                                {
                                    obj = Activator.CreateInstance(tp);
                                    IWrapper wrapper = obj as IWrapper;

                                    Debug.Assert(wrapper != null);

                                    _wrapperCollection.Add(wrapper);
                                }
                                catch (Exception ex2)
                                {
                                    NUnit.UiKit.UserMessage.DisplayFailure(ex2.Message);
                                }
                            }
                        }
                    }

                }
                return _wrapperCollection;
            }
        } private WrapperCollection _wrapperCollection;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wrapperNames"></param>
        public void EnabledWrapper(List<string> wrapperTypeNames)
        {
            foreach (string i in wrapperTypeNames)
            {
                foreach (IWrapper w in this.WrapperCollection)
                {
                    if (w.GetType().FullName == i)
                    {
                        w.Enabled = true;
                    }
                }
            }
        }

        /// <summary>
        /// get first enabled wrapper
        /// </summary>
        /// <returns></returns>
        public IWrapper GetWrapper()
        {
            foreach (IWrapper item in WrapperCollection)
            {
                if (item.Enabled)
                {
                    return item;
                }
            }
            return null;
        }
    }
}
