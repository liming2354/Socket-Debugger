﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

namespace SocketClient
{
    /// <summary>
    /// 
    /// </summary>
    public class LogSaver
    {
        /// <summary>
        /// 
        /// </summary>
        private LogManager _logManager;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logManager"></param>
        public LogSaver(LogManager logManager)
        {
            if (logManager == null)
            {
                throw new ArgumentNullException("logManager");
            }
            _logManager = logManager;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Save(string path)
        {
            StreamWriter sw = null;
            string format = "{0},{1},{2},{3}";

            try
            {
                sw = File.CreateText(path);
                string head = string.Format(
                    format,
                    Strings.Time,
                    Strings.Direction,
                    Strings.Length,
                    Strings.DataHex);
                sw.WriteLine(head);

                foreach (LogItem li in _logManager.Items)
                {
                    string line = string.Format(
                        format,
                        li.DT.ToString("yyyy-MM-dd hh:mm:ss.fff"),
                        ConvertArrayToString(li.StoragePoint.ToArray()),
                        li.Bytes.Length,
                        HexStringConverter.Default.ConvertToObject(li.Bytes)
                        );
                    sw.WriteLine(line);
                }
            }
            finally
            {
                sw.Close();
            }
        }

        private string ConvertArrayToString(object[] objs)
        {

            string s = string.Empty;
            foreach (object obj in objs)
            {
                if (obj != null)
                {
                    s += obj.ToString();
                }
            }
            return s;
        }
    }
}
