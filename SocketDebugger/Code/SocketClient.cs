﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace SocketClient
{
    /// <summary>
    /// 
    /// </summary>
    public class SocketClient
    {

        /// <summary>
        /// 
        /// </summary>
        private const int SIZE = 1024;

        #region Connect
        public void Connect(IPAddress ipAddress, UInt16 port, ProtocolType protocolType)
        {
            Connect(ipAddress, port, false, 0, protocolType);
        }
        #endregion //Connect

        public bool IsUseUdp()
        {
            return _socket.ProtocolType == ProtocolType.Udp;
        }

        #region Connect
        public void Connect(IPAddress ipAddress, UInt16 port, UInt16 localPort, ProtocolType protocolType)
        {
            Connect(ipAddress, port, true, localPort, protocolType);
        }
        #endregion //Connect

        #region Connect
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        public void Connect(IPAddress ipAddress, UInt16 port, bool isUseLocalPort, UInt16 localPort, ProtocolType protocolType)
        {
            if (this.IsConnected)
            {
                throw new InvalidOperationException("isconnected");
            }

            try
            {
                if (protocolType == ProtocolType.Tcp)
                {
                    _socket = new Socket(AddressFamily.InterNetwork,
                            SocketType.Stream,
                        //ProtocolType.Tcp);
                            protocolType);
                }
                else
                {
                    _socket = new Socket(AddressFamily.InterNetwork,
                            SocketType.Dgram,
                            protocolType);
                }


                if (isUseLocalPort)
                {
                    EndPoint localEP = new IPEndPoint(IPAddress.Any, localPort);
                    _socket.Bind(localEP);
                }

                EndPoint ep = new IPEndPoint(ipAddress, port);
                Socket.Connect(ep);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
            finally
            {
                if (!_socket.Connected)
                {
                    _socket = null;
                }
            }
        }
        #endregion //Connect

        #region Socket
        /// <summary>
        /// 
        /// </summary>
        private Socket Socket
        {
            get
            {
                if (_socket == null)
                {
                    _socket = new Socket(AddressFamily.InterNetwork,
                            SocketType.Stream,
                            ProtocolType.Tcp);
                }
                return _socket;
            }
        } private Socket _socket;
        #endregion //Socket

        #region Close
        /// <summary>
        /// 
        /// </summary>
        public void Close()
        {
            CloseHelper();
        }
        #endregion //Close

        #region IsConnected
        public bool IsConnected
        {
            get
            {
                if (this._socket != null)
                {
                    return this._socket.Connected;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion //IsConnected

        #region Send
        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        public void Send(byte[] buffer)
        {
            this._socket.Send(buffer);
        }
        #endregion //Send

        #region RemoteEndPoint
        /// <summary>
        /// 
        /// </summary>
        public EndPoint RemoteEndPoint
        {
            get
            {
                return this.Socket.RemoteEndPoint;
            }
        }
        #endregion //RemoteEndPoint

        public StoragePoint CreateRemoteStoragePoint()
        {
            return new StoragePoint(StoragePointType.S, RemoteEndPoint.ToString());
        }

        public StoragePoint CreateLocalStoragePoint()
        {
            return new StoragePoint(StoragePointType.C, LocalEndPoint.ToString());
        }

        #region LocalEndPoint
        /// <summary>
        /// 
        /// </summary>
        public EndPoint LocalEndPoint
        {
            get
            {
                return this.Socket.LocalEndPoint;
            }
        }
        #endregion //LocalEndPoint

        #region ReceivedBytes
        public byte[] ReceivedBytes
        {
            get
            {
                byte[] bs = _receivedBytes.ToArray();
                //_receivedBytes.Seek(0, SeekOrigin.Begin);
                _receivedBytes.SetLength(0);
                return bs;
            }
        } private MemoryStream _receivedBytes = new MemoryStream();
        #endregion //ReceivedBytes


        #region BeginReceive
        public void BeginReceive()
        {
            if (_socket.ProtocolType == ProtocolType.Tcp)
            {
                BeginReceiveTcp();
            }
            else
            {
                BeginReceiveUdp();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public void BeginReceiveTcp()
        {
            AsyncCallback cb = new AsyncCallback(ReceiveCallbackTcp);
            byte[] receiveBuffer = new byte[SIZE];
            IAsyncResult ia = _socket.BeginReceive(
                receiveBuffer,
                0,
                SIZE,
                SocketFlags.None,
                cb,
                receiveBuffer);
        }

        public void BeginReceiveUdp()
        {
            EndPoint remoteEP = new IPEndPoint(IPAddress.Any, 0);
            byte[] buffer = new byte[SIZE];
            _socket.BeginReceiveFrom(buffer, 0, SIZE, SocketFlags.None, ref remoteEP, new AsyncCallback(EndReceiveUdp), buffer);
        }
        #endregion //BeginReceive

        private void EndReceiveUdp(IAsyncResult ar)
        {
            EndPoint endPoint = new IPEndPoint(IPAddress.Any, 0);
            int n = 0;

            try
            {
                n = _socket.EndReceiveFrom(ar, ref endPoint);
            }
            catch (ObjectDisposedException objectDisposedEx)
            {
                Console.WriteLine("object disposed exception: " + objectDisposedEx.Message);
                return;
            }
            catch (SocketException socketEx)
            {
                Console.WriteLine("socket exception: " + socketEx.Message);
                this.CloseHelper();
                return;
            }

            if (n > 0)
            {
                _receivedBytes.Write((byte[])ar.AsyncState, 0, n);
                if (this.ReceivedEvent != null)
                {
                    ReceivedEvent(this, EventArgs.Empty);
                }

                BeginReceiveUdp();
            }
        }

        #region ReceiveCallback
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ia"></param>
        void ReceiveCallbackTcp(IAsyncResult ia)
        {
            Console.WriteLine("ReceiveCallBack");
            byte[] receiveBuffer = (byte[])ia.AsyncState;
            int n = 0;
            try
            {
                // null ref ex
                //
                n = this._socket.EndReceive(ia);
            }
            catch (ObjectDisposedException objectDisposedEx)
            {
                Console.WriteLine("object disposed exception: " + objectDisposedEx.Message);
                return;
            }
            catch (SocketException socketEx)
            {
                Console.WriteLine("socket exception: " + socketEx.Message);
                this.CloseHelperTcp();
                return;
            }

            if (n > 0)
            {
                _receivedBytes.Write(receiveBuffer, 0, n);
                if (this.ReceivedEvent != null)
                {
                    ReceivedEvent(this, EventArgs.Empty);
                }

                BeginReceiveTcp();
            }
            else
            {
                CloseHelperTcp();
            }
        }
        #endregion //ReceiveCallback

        #region CloseHelper
        private void CloseHelper()
        {
            if (IsUseUdp())
            {
                CloseHelperUdp();
            }
            else
            {
                CloseHelperTcp();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void CloseHelperTcp()
        {
            Console.WriteLine("CloseHelper");

            //if (_socket.Connected)
            {
                _socket.Disconnect(false);
                _socket.Shutdown(SocketShutdown.Both);
                _socket.Close();
                //_socket = null;

                if (this.ClosedEvent != null)
                {
                    this.ClosedEvent(this, EventArgs.Empty);
                }
            }
        }

        private void CloseHelperUdp()
        {
            _socket.Close();
            if (this.ClosedEvent != null)
            {
                this.ClosedEvent(this, EventArgs.Empty);
            }
        }
        #endregion //CloseHelper

        #region Event
        /// <summary>
        /// 
        /// </summary>
        public event EventHandler ReceivedEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler ClosedEvent;

        /// <summary>
        /// 
        /// </summary>
        public event EventHandler ConnectedEvent;
        #endregion //Event
    }
}
